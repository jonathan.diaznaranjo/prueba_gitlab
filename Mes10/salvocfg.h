/****************************************************************************** 
(C)Copyright Pumpkin, Inc. Freely distributable.

$Source: C:\\RCS\\D\\Pumpkin\\CubeSatKit\\Example\\PIC24\\PIC24FJ256GA110\\CubeSat_Kit_Dev_Board\\Test\\Test1\\MPLAB_C30\\Pro-lib\\salvocfg.h,v $
$Author: aek $
$Revision: 3.2 $
$Date: 2010-01-24 20:39:50-08 $

******************************************************************************/

// Salvo configuration file

#define OSEVENTS                    2
#define OSEVENT_FLAGS               0
#define OSMESSAGE_QUEUES            0
#define OSTASKS                     20

#define OSENABLE_BINARY_SEMAPHORES  TRUE
#define OSENABLE_TIMEOUTS           TRUE
#define OSENABLE_IDLING_HOOK        TRUE
#define OSBYTES_OF_DELAYS           4  //16-bit delays
#define OSBYTES_OF_TICKS            4  //32-bit OStimerTicks
