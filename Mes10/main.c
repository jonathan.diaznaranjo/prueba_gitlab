/* 
 * File:   main.c
 * Author: Usuario
 *
 * Created on 7 de marzo de 2022, 08:40 AM
 */


#include <salvo.h>
#include "xc.h"
#include "p24FJ128GA204.h"
/*
 * 
 */
int counterTaskA, counterTaskB=0;

void TaskA (void){
    
  
    while(1){
        counterTaskA++;
        
        LATCbits.LATC5 ^= 1 ;  // Toggle Red Led
        int i;
        for (i = 0; i < 32000; i++) ;
        for (i = 0; i < 32000; i++) ;
        //OS_Delay(100);
         OS_Yield();
    }
}

void TaskB (void){
   
    while(1){
        counterTaskB++;
        LATCbits.LATC5 ^= 1;  // Toggle Red Led
        int i;
        for (i = 0; i < 32000; i++) ;
        for (i = 0; i < 32000; i++) ;
      //  OS_Delay(100); 
        OS_Yield();
    }
}


int main(void) {
   // TRISC.F5=0;
 TRISCbits.TRISC5 = 0x00; // Port C, pin 5 as output
  
 // Initialize Salvo RTOS.
 OSInit();

  // Create tasks.
 OSCreateTask(TaskA, OSTCBP(1), 1);
 OSCreateTask(TaskB, OSTCBP(2), 5);
    
  // Enable interrupts (enables UART tx & rx).
  //__enable_interrupt();
 
    while(1){
    OSSched();    
    }
    
}

